const companyController = {
  companyList: [
    { id: 1, name_company: 'Felidas', description_company: 'Company Registration since 2020' },
    { id: 2, name_company: 'Storie', description_company: 'Company Registration since 2019' }
  ],
  lastId: 3,
  addCompany (company) {
    company.id = this.lastId++
    this.companyList.push(company)
    return company
  },
  updateCompany (company) {
    const index = this.companyList.findIndex(item => item.id === company.id)
    this.companyList.splice(index, 1, company)
    return company
  },
  deleteCompany (id) {
    const index = this.companyList.findIndex(item => item.id === parseInt(id))
    this.companyList.splice(index, 1)
    return { id }
  },
  getCompanys () {
    return [...this.companyList]
  },
  getCompany (id){
    const company = this.companyList.find(item => item.id === parseInt(id))
    return company
  },
}
module.exports = companyController
