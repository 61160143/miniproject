const mongoose = require('mongoose')
const Company = require('./model/Company')

mongoose.connect('mongodb://admin:password@localhost/mydb', {
   useNewUrlParser: true, 
   useUnifiedTopology: true 
  })

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

Company.find(function (err, companys) {
  if (err) return console.error(err)
  console.log(companys)
})