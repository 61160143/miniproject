const express = require('express')
const router = express.Router()
const workpositionController = require('../controller/WorkPositionController')

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.json(workpositionController.getWorkPositions())
})

router.get('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(workpositionController.getWorkPosition(id))
})

router.post('/', (req, res, next) => {
  const payload = req.body
  res.json(workpositionController.addWorkPosition(payload))
})

router.put('/', (req, res, next) => {
  const payload = req.body
  res.json(workpositionController.updateWorkPosition(payload))
})

router.delete('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(workpositionController.deleteWorkPosition(id))
})

module.exports = router
