const express = require('express')
const router = express.Router()
const companyController = require('../controller/CompanyController')

/* GET users listing. */
router.get('/', (req, res, next) => {
  res.json(companyController.getCompanys())
})

router.get('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(companyController.getCompany(id))
})

router.post('/', (req, res, next) => {
  const payload = req.body
  res.json(companyController.addCompany(payload))
})

router.put('/', (req, res, next) => {
  const payload = req.body
  res.json(companyController.updateCompany(payload))
})

router.delete('/:id', (req, res, next) => {
  const { id } = req.params
  res.json(companyController.deleteCompany(id))
})

module.exports = router
