const mongoose = require('mongoose')
const Schema = mongoose.Schema
const compannySchema = new Schema({
  name: String,
  companydescription: String
})

module.exports = mongoose.model('Company', compannySchema)