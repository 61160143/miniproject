const workpositionService = {
  workpositionList: [
    { id: 1, name_position: 'Sales Officer', description_position: 'The best strategies to increase customer purchases.' },
    { id: 2, name_position: 'Engineer', description_position: 'Engineers will design and draft blueprints' }
  ],
  lastId: 3,
  addWorkPosition (workposition) {
    workposition.id = this.lastId++
    this.workpositionList.push(workposition)
  },
  updateWorkPosition (workposition) {
    const index = this.workpositionList.findIndex(item => item.id === workposition.id)
    this.workpositionList.splice(index, 1, workposition)
  },
  deleteWorkPosition (workposition) {
    const index = this.workpositionList.findIndex(item => item.id === workposition.id)
    this.workpositionList.splice(index, 1)
  },
  getWorkPositions () {
    return [...this.workpositionList]
  }
}
export default workpositionService
