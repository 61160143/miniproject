const workpositionService = {
  workpositionList: [
    { id: 1, name_position: 'Sales Officer', description_position: 'The best strategies to increase customer purchases.' },
    { id: 2, name_position: 'Engineer', description_position: 'Engineers will design and draft blueprints' }
  ],
  lastId: 3,
  addWorkPosition (workposition) {
    workposition.id = this.lastId++
    this.workpositionList.push(workposition)
    return workposition
  },
  updateWorkPosition (workposition) {
    const index = this.workpositionList.findIndex(item => item.id === workposition.id)
    this.workpositionList.splice(index, 1, workposition)
    return workposition
  },
  deleteWorkPosition (id) {
    const index = this.workpositionList.findIndex(item => item.id === parseInt(id))
    this.workpositionList.splice(index, 1)
    return { id }
  },
  getWorkPositions () {
    return [...this.workpositionList]
  },
  getWorkPosition (id){
    const workposition = this.workpositionList.find(item => item.id === parseInt(id))
    return workposition
  }
}
module.exports = workpositionService
